#!/bin/bash

# $1 : .tar file for optimizations
# $2 : deodex'ed firmware path

if [ ! -e $1 ]; then
	echo "Input tar file doesn't exists!!"
	exit 1
fi
if [ ! -e $2 ]; then
	echo "Deodex'ed firmware path doesn't exists!!"
	exit 1
fi

random=$(date +%s)$(uuidgen | sed s/-//g)
mkdir tmp.$random
echo "Working on tmp.$random"

cd tmp.$random

# Extract firmware
tar -xf "$1"
rm boot.img
imgname=$(\ls -d system.img*)
simg2img $imgname new.img || mv $imgname new.img
mkdir mount
mount new.img mount
rsync -a mount/ tmp/

# Capture permissions and SELinux contexts
cd mount/
getfacl -RP . > ../facl
# This command may not work or differ from your computer date formatting settings
find . -exec ls -aldZ {} \; | grep -v u:object_r:system_file:s0 | grep -v u:object_r:system_library_file:s0 | while read file; do echo chcon -h $(echo $file | awk '{print $5}') $(echo $file | awk '{print $10}'); done > ../selinux
cd ../

# Take care of CSC
simg2img cache.img.ext4 csc.img || simg2img cache.img csc.img
mkdir cache csc
mount csc.img cache

cd csc
7z x ../cache/recovery/sec_csc.zip
cp -rp system/* ../tmp/
# Remove SystemUI Multitasking fixed-app list
find ../tmp/csc -name feature.xml | while read file; do cat $file | grep -v "CscFeature_Framework_ShortCutListInRecentApp" > tmp; mv tmp $file; done
cd ..

cd tmp

# copy back deodex'ed files
find . | grep odex | while read file; do rm $file; done
find . -name arm -exec rmdir {} \; 2>/dev/null
find . -name arm64 -exec rmdir {} \; 2>/dev/null
rm -rf framework/arm*
find . -name '*.apk' -exec cp $2/{} {} \;
find . -name '*.jar' -exec cp $2/{} {} \;

# Remove KNOX, OTA and other unnecessary files
cat /usr/artware/remove.txt | while read file; do
	rm -rf $file
done

# Install frameworks for modifications
cd framework
apktool if framework-res.apk
apktool if twframework-res.apk
cd ..

# arter97 signature
sed -i -e 's/buildinfo.sh/buildinfo.sh\n# released by arter97/g' build.prop

# Other Knox & SecureStorage stuffs
sed -i -e '/ro.securestorage.knox/c\ro.securestorage.knox=false' build.prop
sed -i -e '/ro.securestorage.support/c\ro.securestorage.support=false' build.prop

# Enable Multi-user
echo "fw.max_users=3
fw.show_multiuserui=1" >> build.prop

# Patch permission for external storage access
echo '--- a/etc/permissions/platform.xml
+++ b/etc/permissions/platform.xml
@@ -69,6 +69,7 @@
     <permission name="android.permission.WRITE_EXTERNAL_STORAGE" >
         <group gid="sdcard_r" />
         <group gid="sdcard_rw" />
+        <group gid="media_rw" />
     </permission>
 
     <permission name="android.permission.ACCESS_ALL_EXTERNAL_STORAGE" >' | patch -p1
rm etc/permissions/platform.xml.* 2>/dev/null

# Patch framework-res.apk for smooth spinner animations
cd framework
apktool d -s framework-res.apk
patch -p1 < /usr/s5/framework-res.patch
cd framework-res
find . -type f -name '*.PNG' | while read file; do mv -f $file $(echo "$file" | tr '[:upper:]' '[:lower:]'); done
apktool b -c .
cp dist/framework-res.apk ../
cd ..
rm -rf framework-res
cd ..

# Patch services.jar for ALMI(Android Lollipop Memory-leak Improvements)
# and 0.75x animation scale
cd framework
apktool d -r services.jar
cd services.jar.out
cp /usr/s5/prebuilt/services.jar.out/smali/com/android/server/display/ColorFade.smali smali/com/android/server/display/ColorFade.smali
# mWindowAnimationScaleSetting
sed -i -e '0,/0x3f800000/s//0x3f400000/' smali/com/android/server/wm/WindowManagerService.smali
# mTransitionAnimationScaleSetting
sed -i -e '0,/0x3f800000/s//0x3f400000/' smali/com/android/server/wm/WindowManagerService.smali
# mAnimatorDurationScaleSetting
sed -i -e '0,/0x3f800000/s//0x3f400000/' smali/com/android/server/wm/WindowManagerService.smali
apktool b -c .
cp dist/services.jar ../
cd ..
rm -rf services.jar.out
cd ..

# Replace boot animations and audio with international ones
rm media/audio/ui/PowerOff.ogg 2>/dev/null
if grep -q "ro.sf.lcd_density=640" build.prop ; then
	# QHD
	cp -rp /usr/artware/prebuilt/media/qhd/* media/
else
	cp -rp /usr/artware/prebuilt/media/fhd/* media/
fi

# Strip ARM binaries
striparm
striparm64

# Optimize zip type files
cd framework
find . -name '*.jar' | parallel 0zipa > /dev/null
find . -name '*.apk' | parallel 0zipa > /dev/null
cd ../priv-app
find . -name '*.jar' | parallel 0zipa > /dev/null
find . -name '*.apk' | parallel 0zipa > /dev/null
# Optimize everything and compress ones which is big until /system size enters the comfort zone.
cd ../app
find . -name '*.jar' | parallel 0zipa > /dev/null
find . -name '*.apk' | parallel 0zipa > /dev/null
find . -name '*.jar' >> list
find . -name '*.apk' >> list
find ../priv-app -name '*.jar' >> list
find ../priv-app -name '*.apk' >> list
cat /usr/artware/compress.txt | while read file; do
	find .. -name "*${file}*.apk"
done >> list
cat list | uniq > tmp
mv tmp list
cat /usr/s5/optimize.txt | while read file; do
	find .. -name "*${file}*" 2>/dev/null | while read opt_file; do
		if [ -e $opt_file/*.apk ]; then
			cat list | grep -v $(basename $opt_file/*.apk) > tmp
			mv tmp list
		fi
		if [ -f $opt_file ]; then
			cat list | grep -v $(basename $opt_file) > tmp
			mv tmp list
		fi
	done
done
cat list | while read a; do readlink -m $a; done | sort | uniq > tmp
cat tmp | while read a; do du -sb $a; done | sort -g | awk '{print $2, $3, $4, $5, $6, $7, $8, $9}' | tac > list
rm tmp
UPDIR=$(cd ../ ; pwd)
sed -i -e "s@$UPDIR/@@g" -e 's/[[:space:]]*$//' list
if [[ $imgname == "system.img" ]]; then
	# Exynos uses sparse type image.
	# If the available space left on system.img is low, ODIN may fail.
	# So try to match the available space from the stock image.
	sizelimit=$(du -sb ../../mount/ | awk '{print $1}')
else
	# Others who can flash a full ext4 system.img don't need to worry about the available space.
	# But leave at least 25 MB for users.
	cat /dev/zero > ../../mount/fill 2>/dev/null
	sizelimit=$(($(du -sb ../../mount/ | awk '{print $1}') - 26214400))
	rm ../../mount/fill
fi
until [ $(du -sb .. | awk '{print $1}') -le $sizelimit ]; do
	echo "Compressing $(head -n 1 list) due to overhead"
	9zipa ../$(head -n 1 list) > /dev/null
	tail -n +2 list > tmp
	mv tmp list
	if [ ! -s list ]; then
		echo "Failed to perform firmware diet!!"
		exit 1
	fi
done
rm tmp list 2>/dev/null
cd ../

# SHV-E300 fails to properly use StoryAlbum
# Copy one from Samsung Apps.
if echo "$@" | grep -q E300 ; then
	rm -rf priv-app/StoryAlbum*
	cp -rp /usr/artware/prebuilt/copy_e300/* .
fi

cd ../
rm -rf mount/*
cp -rp tmp/* mount/

# Fix permissions
cd mount/
find . -type d | while read file; do
	chown 0.0 $file
	chmod 755 $file
done
find . -type f | while read file; do
	chown 0.0 $file
	chmod 644 $file
done
setfacl --restore=../facl

# Fix SELinux contexts
find . | while read file; do
	chcon -h u:object_r:system_file:s0 $file
done
find lib* | while read file; do
	chcon -h u:object_r:system_library_file:s0 $file
done
. ../selinux

# Defrag ext4 image
e4defrag .
cat /dev/zero > fill 2>/dev/null
e4defrag .
rm fill
cd ..
umount mount
# fsck, optimize ext4 image
e2fsck -y -f -D new.img

# Prepare for release
if [[ $imgname == "system.img" ]]; then
	# Exynos
	ext2simg new.img simg.img
	rm $imgname
	sgs4ext4fs --bloat simg.img $imgname
else
	mv new.img $imgname
fi
chmod 644 $imgname
TARNAME=$(echo $(basename $1) | tr '_' ' ' | awk '{print $1}')_$(echo $(basename $1) | tr '_' ' ' | awk '{print $2}')_$(echo $(basename $1) | tr '_' ' ' | awk '{print $3}')-$(date +%F | sed s@-@@g)-SYSTEM-arter97.tar
tar -H ustar -c $imgname > $TARNAME
md5sum -t $TARNAME >> $TARNAME
mv $TARNAME ../$TARNAME.md5
cd ../
umount */* 2>/dev/null
rm -rf tmp.$random
